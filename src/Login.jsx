import React, { Component } from 'react';
import firebase from 'firebase';

import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';

export default class Login extends Component {
    state = {
        email: '',
        password: ''
    }

    constructor(props) {
        super(props)
    }

    /**
     * ログイン処理
     */
    loginAuth = () => {
        const { email, password } = this.state
        firebase.auth().signInWithEmailAndPassword(email, password).then(() => {

        }, () => {
            this.setState({
                message: 'ログインに失敗しました',
            })
        })
    }

    /**
     * ユーザ作成処理
     */
    createUser = () => {
        const { email, password } = this.state
        firebase.auth().createUserWithEmailAndPassword(email, password).then(user => {
            const newUser = {
                name: email,
                email: email,
                enter_datetime: firebase.database.ServerValue.TIMESTAMP,
            }
            firebase.database().ref('users/' + user.uid).set(newUser).then()
        })
    }

    render() {
        const {
            email,
            password
        } = this.state

        return (
            <div>
                <TextField
                    hintText="ログインID"
                    type='email'
                    value={email}
                    onChange={e => {
                        const value = e.target.value
                        this.setState({email: value})
                    }}

                /><br/>
                <TextField
                    hintText="パスワード"
                    type="password"
                    value={password}
                    onChange={e => {
                        const value = e.target.value
                        this.setState({password: value})
                    }}
                /><br/>
                <RaisedButton
                    label="ログイン"
                    onTouchTap={() => this.loginAuth()}
                /><br/>
                <RaisedButton
                    label="ユーザ作成"
                    onTouchTap={() => this.createUser()}
                />
            </div>
        );
    }
}
