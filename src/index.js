import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { HashRouter as Router } from 'react-router-dom'

import { firebaseApp, firebaseDb } from './firebase'
import './index.css'
import App from './App'

import injectTapEventPlugin from 'react-tap-event-plugin'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import lightBaseTheme from 'material-ui/styles/baseThemes/lightBaseTheme'
import getMuiTheme from 'material-ui/styles/getMuiTheme'
injectTapEventPlugin()


ReactDOM.render(
    <div>
        <Router>
            <MuiThemeProvider muiTheme={getMuiTheme(lightBaseTheme)}>
                <App />
            </MuiThemeProvider>
        </Router>
    </div>,
    document.getElementById('root')
)