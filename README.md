# react Training
# react(dom)の導入〜ユーザログイン、作成まで

## Process
### `Create React App`
- npm create-app-react AppName
- npm install
    - react
    - react-dom
    - material-ui

### `Connect Firebase Hosting`
- Make firebase project
- npm install -g firebase-tools
- firebase login
- firebase init
    - Space -> Hosting
    - 基本的に全てEnter
- firebase deploy

### `Connect Firebase Database`
- npm install firebase --save
- mkdir firebase
- touch config.js
    - 接続情報を入力
- touch index.js
    - インスタンス化
    - ファイルインポートでデータベースアクセスを可能に

### `Create User Logic`
- firebase.auth().`createUserWithEmailAndPassword`(email, password).then()

### `Login Attempt User Logic`
- firebase.auth().`signInWithEmailAndPassword`(email, password).then()

### `Database set on Firebase`
- firebase.database().ref('users/' + uid).`set`({name: name, email: email}).then()

## Sites
https://qiita.com/gonta616/items/278a7e81a8b624d9621e

## Error Site
https://qiita.com/foloinfo/items/4dd913b80967ed3357c2

